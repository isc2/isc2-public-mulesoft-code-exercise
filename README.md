(ISC)² Interview Mulesoft Coding Exercise
This repository contains a coding exercise for use in the hiring process for Mulesoft developers / Solution Engineers.

The idea behind the exercise is to assess general knowledge of mule applications - their structure and capabilities.

Instructions for Candidates
To get started, download the .jar file and import it into Anypoint Studio
The ultimate goal of this task is to get this application to a working state. 
The application should listen for HTTP requests and write the data to a .csv.
The only strict expectations are:
 A working application
 Interface, implementation and configuration xml separated appropriately.
 Property placeholders used appropriately.
 The rest is up to you!
This exercise is open book and you may use any resources online you wish to.
When completed, export your project as a deployable archive to a Git repository of your own (Bitbucket, Github, GitLab, etc.)
and provide the URL and any manual instructions necessary to run the application successfully to your interviewer.